<?php

/*
Site : http:www.smarttutorials.net
Author :muni
*/
require_once 'config.php';

if( isset($_POST['type']) && !empty($_POST['type'] ) ){
	$type = $_POST['type'];
	
	switch ($type) {
		case "save_usuario":
			save_usuario($mysqli);
			break;
		case "delete_usuario":
			delete_usuario($mysqli, $_POST['id']);
			break;
		case "getusuarios":
			getusuarios($mysqli);
			break;
		default:
			invalidRequest();
	}
}else{
	invalidRequest();
}

/**
 * This function will handle usuario add, update functionality
 * @throws Exception
 */

function save_usuario($mysqli){
	try{
		$data = array();
		$user = $mysqli->real_escape_string(isset( $_POST['usuario']['user'] ) ? $_POST['usuario']['user'] : '');
		$password = $mysqli->real_escape_string(isset( $_POST['usuario']['password'] ) ? $_POST['usuario']['password'] : '');
		$name = $mysqli->real_escape_string( isset( $_POST['usuario']['name'] ) ? $_POST['usuario']['name'] : '');
		$shortname = $mysqli->real_escape_string( isset( $_POST['usuario']['shortname'] ) ? $_POST['usuario']['shortname'] : '');
		$turno = $mysqli->real_escape_string( isset( $_POST['usuario']['turno'] ) ? $_POST['usuario']['turno'] : '');
		$invpros = $mysqli->real_escape_string( isset( $_POST['usuario']['invpros'] ) ? $_POST['usuario']['invpros'] : '');
		$admin = $mysqli->real_escape_string( isset( $_POST['usuario']['admin'] ) ? $_POST['usuario']['admin'] : '');
		$tablerocontrol = $mysqli->real_escape_string( isset( $_POST['usuario']['tablerocontrol'] ) ? $_POST['usuario']['tablerocontrol'] : '');
		$equipo = $mysqli->real_escape_string( isset( $_POST['usuario']['equipo'] ) ? $_POST['usuario']['equipo'] : '');
	
		if($user == '' || $password == '' || $name == '' || $invpros == '' || $admin == '' || $tablerocontrol == ''){
			throw new Exception( "Campos requeridos faltantes" );
		}
		
		$query = "DELETE FROM usuarios WHERE user = '$user'; INSERT INTO usuarios (user, password, name, shortname, turno, invpros, admin, tablerocontrol, equipo) VALUES ('$user', '$password', '$name', '$shortname', '$turno', '$invpros', '$admin', '$tablerocontrol', '$equipo')";

		if( $mysqli->multi_query( $query ) ){
			$data['success'] = true;
			$data['message'] = 'Usuario insertado/actualizado exitosamente.';
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		$mysqli->close();
		echo json_encode($data);
		exit;
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

/**
 * This function will handle usuario deletion
 * @param string $id
 * @throws Exception
 */

function delete_usuario($mysqli, $idUsuario = ''){
	try{
		if(empty($id)) throw new Exception( "Clave de c inválido." );
		$query = "DELETE FROM `usuarios` WHERE `idUsuario` = $idUsuario";
		if($mysqli->query( $query )){
			$data['success'] = true;
			$data['message'] = 'Usuario eliminado exitosamente.';
			echo json_encode($data);
			exit;
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

/**
 * This function gets list of usuarios from database
 */
function getusuarios($mysqli){
	try{
	
		$query = "SELECT * FROM `usuarios` order by user desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}


function invalidRequest()
{
	$data = array();
	$data['success'] = false;
	$data['message'] = "Opción inválida.";
	echo json_encode($data);
	exit;
}

function debug_to_console( $data ) {

    if ( is_array( $data ) )
        $output = "<script>console.log( 'Debug Objects: " . implode( ',', $data) . "' );</script>";
    else
        $output = "<script>console.log( 'Debug Objects: " . $data . "' );</script>";

    echo $output;
}



