'use strict';
/**
 * controllers for ng-table
 * Simple table with sorting and filtering on AngularJS
 */
app.controller('ngTableCtrl_Usuarios', ["$scope", "$filter", "ngTableParams", "$http",  "$modal", "$log", "SweetAlert", function ($scope, $filter, ngTableParams, $http, $modal, $log, SweetAlert) {
    var base_path = document.getElementById('base_path').value;

    $scope.post = {};
    $scope.post.Usuarios = [];
    $scope.tempUsuario = {};
    $scope.editMode = false;
    $scope.index = '';

    var url = base_path+'assets/js/php/usuarios_DB.php';

    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 10, // count per page
        sorting: {
            user: 'asc' // initial sorting
        },
        filter: {
            user: '' // initial filter
        }
    }, {
        total: $scope.post.Usuarios.length, // length of Validaciones
        getData: function ($defer, params) {
            // use build-in angular filter
            var filteredData = params.filter() ? $filter('filter')($scope.post.Usuarios, params.filter()) : $scope.post.Usuarios;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });

    $scope.saveusuario = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({'usuario' : $scope.tempUsuario, 'type' : 'save_usuario' }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                if( $scope.editMode ){
                    $scope.post.Usuarios[$scope.index].user = $scope.tempUsuario.user;
                    $scope.post.Usuarios[$scope.index].password = $scope.tempUsuario.password;
                    $scope.post.Usuarios[$scope.index].name = $scope.tempUsuario.name;
                    $scope.post.Usuarios[$scope.index].shortname = $scope.tempUsuario.shortname;
                    $scope.post.Usuarios[$scope.index].turno = $scope.tempUsuario.turno;
                    $scope.post.Usuarios[$scope.index].invpros = $scope.tempUsuario.invpros;
                    $scope.post.Usuarios[$scope.index].admin = $scope.tempUsuario.admin;
                    $scope.post.Usuarios[$scope.index].tablerocontrol = $scope.tempUsuario.tablerocontrol;
                    $scope.post.Usuarios[$scope.index].equipo = $scope.tempUsuario.equipo;
                }else{
                    $scope.post.Usuarios.push({
                        user : $scope.tempUsuario.user,
                        password : $scope.tempUsuario.password,
                        name : $scope.tempUsuario.name,
                        shortname : $scope.tempUsuario.shortname,
                        turno : $scope.tempUsuario.turno,
                        invpros : $scope.tempUsuario.invpros,
                        admin : $scope.tempUsuario.admin,
                        tablerocontrol : $scope.tempUsuario.tablerocontrol,
                        equipo : $scope.tempUsuario.equipo,
                    });
                    $scope.tableParams.reload();
                }
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.tempUsuario = {};
                $scope.stopwaiting();
            }else{
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }).
        error(function(data, status, headers, config) {
            //$scope.codeStatus = response || "Request failed";
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
        
//        jQuery('.btn-save').button('reset');
    }

    $scope.addusuario = function(){
        
        jQuery('.btn-save').button('loading');
        $scope.saveusuario();
        $scope.editMode = false;
        $scope.index = '';
    }
    
    $scope.updateusuario = function(){
        $('.btn-save').button('loading');
        $scope.saveusuario();
    }
    
    $scope.editusuario = function(usuario){
        $scope.tempUsuario = {
            user: usuario.user,
            password : usuario.password,
            name : usuario.name,
            shortname : usuario.shortname,
            turno : usuario.turno,
            invpros : usuario.invpros,
            admin : usuario.admin,
            tablerocontrol : usuario.tablerocontrol,
            equipo : usuario.equipo
        };
        $scope.editMode = true;
        $scope.index = $scope.post.Usuarios.indexOf(usuario);
    }
    
    
    $scope.deleteusuario = function(usuario){
        var r = confirm("Are you sure want to delete this usuario!");
        if (r == true) {
            $http({
              method: 'post',
              url: url,
              data: $.param({ 'idUsuario' : usuario.idUsuario, 'type' : 'delete_usuario' }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            success(function(data, status, headers, config) {
                if(data.success){
                    var index = $scope.post.Usuarios.indexOf(usuario);
                    $scope.post.Usuarios.splice(index, 1);
                }else{
                    $scope.messageFailure(data.message);
                }
            }).
            error(function(data, status, headers, config) {
                //$scope.messageFailure(data.message);
            });
        }
    }
    
    $scope.init = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'getusuarios' }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success && !angular.isUndefined(data.data) ){
                $scope.post.Usuarios = data.data;
                $scope.tableParams.total($scope.post.Usuarios.length);
                $scope.tableParams.reload();
            }
            $scope.stopwaiting();
        }).
        error(function(data, status, headers, config) {
            //$scope.messageFailure(data.message);
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }
    
    $scope.messageFailure = function (msg){
        jQuery('.alert-failure-div > p').html(msg);
        jQuery('.alert-failure-div').show();
        jQuery('.alert-failure-div').delay(5000).slideUp(function(){
            jQuery('.alert-failure-div > p').html('');
        });
    }
    
    $scope.messageSuccess = function (msg){
        jQuery('.alert-success-div > p').html(msg);
        jQuery('.alert-success-div').show();
        jQuery('.alert-success-div').delay(5000).slideUp(function(){
            jQuery('.alert-success-div > p').html('');
        });
    }
    
    
    $scope.getError = function(error, name){
        if(angular.isDefined(error)){
        }
    }

    $scope.open = function (usuario,editMode,size) {

        if(editMode) {
            $scope.tempUsuario = {
                user : usuario.user,
                password : usuario.password,
                name : usuario.name,
                shortname : usuario.shortname,
                turno : usuario.turno,
                invpros : usuario.invpros,
                admin : usuario.admin,
                tablerocontrol : usuario.tablerocontrol,
                equipo : usuario.equipo,
            };
        } else {
            $scope.tempUsuario = {
                user : "",
                password : "",
                name : "",
                shortname : "",
                turno : "",
                invpros : "NO",
                admin : "NO",
                tablerocontrol : "NO",
                equipo : "",
            };
        };
        $scope.editMode = editMode;
        $scope.index = $scope.post.Usuarios.indexOf(usuario);

        var modalInstance = $modal.open({
            templateUrl: 'EditarUsuario.html',
            controller: 'ModalInstanceCtrl',
            scope : $scope,
            size: size
        });

        modalInstance.result.then(function () {
            $scope.saveusuario();
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.waiting = function() {
        $scope.wait = "csspinner load1";
    }

    $scope.stopwaiting = function() {
        $scope.wait = "";
    }

}]);

app.controller('ModalInstanceCtrl', ["$scope", "$modalInstance", function ($scope, $modalInstance) {

    $scope.ok = function () {
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]);