<!-- start: PAGE TITLE -->
<style>
.rojo {
    color: #ff6666;
}
.amarillo {
    color: #cccc00;
}
.azul {
    color: #4db8ff;
}
.verde {
    color: #00cc00;
}
.azul2 {
    color: #0000ff;
}
.morado {
    color: #5900b3;
}
.crema {
    color: #ff66ff;
}
.gris {
    color: #d1d1e0;
}
.negro {
    color: #000000;
}
.oro {
    color: #808000;
}
.naranja {
    color: #ff9933;
}
.selected {
    font-weight:bold;
    background-color: #ccffcc;
}
</style>
<section id="page-title" class="padding-top-15 padding-bottom-15"  ng-controller="darhe">
	<div class="row">
		<div class="col-sm-12">
			<h1 class="mainTitle">Tablero de Control</h1>
			<span class="mainDescription">Totales x Tipificación </span>
		</div>
	</div>
</section>
<!-- end: PAGE TITLE -->
<!-- start: CONSULTA DE ExpedientesCiviles -->
<section ng-controller="darhe" ng-init="init_tipif2()">
	<div class="row">
		<div class="col-md-3" ng-repeat="(key, value) in post.TotalXTipif | groupBy: 'nombre'">
			<div class="panel panel-white {{wait}}">
				<div class="panel-heading border-light bg-orange">
					<h4 class="panel-title"><b>{{ key }}</b></h4>
				</div>
				<div class="panel-body">
					<table style="width:100%">
						<tr>
							<td><b>ESTATUS</b></td>
							<td align="right"><b>TOTAL</b></td>
							<td align="right"v><b>%</b></td>
						</tr>
						<tr ng-repeat="renglon in value">
							<td>{{ renglon.desestatus }}</td>
							<td align="right">{{ renglon.total }}</td>
							<td align="right">{{ renglon.porcentaje | number: 2 }}</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<!--
		<div class="col-md-3">
			<div class="panel panel-white {{wait}}">
				<div class="panel-heading border-light bg-orange">
					<h4 class="panel-title"><b>Total</b></h4>
				</div>
				<div class="panel-body">
					<table>
						<tr>
							<td>Estatus</td>
							<td>Total</td>
						</tr>
						<tr ng-repeat>
						</tr>
					</table>
						<table ng-table="tableParamsA" show-filter="false" class="table table-condensed table-hover">
							<tr ng-repeat="Agenda in $data" >
								<td data-title="'Empresa'" filter="{ 'desempresa': 'text' }" sortable="'desempresa'" > {{Agenda.desempresa}} </td>
								<td data-title="'Siguiente Contacto'" filter="{ 'fechasiguientecontacto': 'text' }" sortable="'fechasiguientecontacto'" > {{Agenda.fechasiguientecontacto}} </td>
							</tr>
						</table>
				</div>
			</div>
		</div>-->
	</div>

</section>
<!-- end: CONSULTA DE ExpedientesCiviles -->
